## Live version

A live version of the app this API feeds can be found at http://pizza42.gildersleeve.eu/. The backing
API is at http://pizza42-api.gildersleeve.eu:8080/. They are hosted on two EC2 images.

See the SPA's Bitbucket page for more information:
https://bitbucket.org/wgilder/pizza42-poc-spa/src/master/. 

## About

The API leverages the Python Flask mini server, fronted by NGINX and packaged using Docker.

## The API

### Version
http://pizza42-api.gildersleeve.eu:8080/v1/version 

[GET] Unsecured
Shows basic version info for the API

### User information
http://pizza42-api.gildersleeve.eu:8080/v1/allUsers

[GET] Unsecured
Shows all visitors to the site including various profile info. It also shows a breakdown 
of certain user stats. For example:

```
{
  "user_stats": {
    "genders": {
      "female": 0, 
      "male": 1, 
      "unknown": 0
    }, 
    "google_logins": 1, 
    "locales": {
      "en": 1
    }, 
    "total_connections": 153, 
    "total_orders": 0, 
    "unique_users": 1
  }, 
  "users": [
    {
      "connection_count": 153, 
      "email": "wa********@gildersleeve.eu", 
      "gender": "male", 
      "locale": "en", 
      "order_count": 0, 
      "sources": {
        "auth0": 1, 
        "google-oauth2": 1
      }
    }
  ]
}
```

### Pizza selection
http://pizza42-api.gildersleeve.eu:8080/v1/allItems

[GET] Authentication required
Returns all pizzas available for selection.

### Order submission
http://pizza42-api.gildersleeve.eu:8080/v1/allItems

[POST] Authentication required
The pizzas selected in the UI are "ordered."

## Creating the Docker image

```
docker build -t pizza42-poc-api .
docker login --username=$DOCKER_USERNAME
docker tag $IMAGE_ID wgilder/pizza42-poc-api:$PUBLIC_TAG
docker push wgilder/pizza42-poc-api
```

## Running Docker

The application is started using the following commands:

```
mkdir /tmp/users
chmod 777 /tmp/users
sudo chwon root:root /tmp/users
sudo docker pull wgilder/pizza42-poc-spa:initial
sudo docker run -ti --rm -d -p 8080:80 --name pizza42_api -v /tmp/users:/tmp/users wgilder/pizza42-poc-api:initial
```

User information is stored in flat files in the mounted volume.