from flask import Flask, jsonify

class localFlask(Flask):
    def process_response(self, response):
        #Every response will be processed here first
        response.headers['Access-Control-Allow-Origin'] = "*"
        response.headers['Access-Control-Allow-Credentials'] = "true"
        response.headers['Access-Control-Allow-Methods'] = "GET, POST, OPTIONS"
        response.headers['Access-Control-Allow-Headers'] = "DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,Access-Control-Expose-Headers"
        super(localFlask, self).process_response(response)
        return(response)

app = localFlask(__name__)

YOUR_API_AUDIENCE = "http://pizza42-api.gildersleeve.eu"
AUTH0_DOMAIN = 'wgilder.eu.auth0.com'
API_AUDIENCE = YOUR_API_AUDIENCE
ALGORITHMS = ["RS256"]

class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code

@app.errorhandler(AuthError)
def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response

from auth0.pizza42 import routes