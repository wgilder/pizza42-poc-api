
class Pizza(object):
    def __init__(self, id, name, toppings, price):
        self._id = id
        self._name = name
        self._toppings = toppings
        self._price = price

    def get_name(self):
        return self._name

    def get_toppings(self):
        return self._toppings

    def get_price(self):
        return self._price

    def get_id(self):
        return self._id

    def serialize(self):
        return { 'id': self._id, 'name': self._name, 'toppings': self._toppings, 'price': self._price }