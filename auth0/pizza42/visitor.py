import json
import os
import re

_base_dir = "/tmp/users/"

class Visitor(object):
    def __init__(self, email, order_count, sources, connection_count, gender, locale):
        self.email = email
        self.sources = {}

        if (sources):
            for key,val in sources.items():
                self.sources[key] = val

        self.order_count = order_count
        self.connection_count = connection_count
        self.gender = gender
        self.locale = locale

    def serialize(self, obfuscate=False):
        email = _obfuscate(self.email) if obfuscate else self.email
        return { 'email': email, 'order_count': self.order_count, 'sources': self.sources, 'connection_count': self.connection_count, 'gender': self.gender, 'locale': self.locale }

def inc_order(email):
    usr = read_user(email)
    usr.order_count += 1
    write_user(usr)

def add_origin(email, src):
    usr = read_user(email)
    usr.sources[src] = 1
    write_user(usr)

def add_misc_info(email, gender, locale):
    usr = read_user(email)
    usr.gender = gender
    usr.locale = locale
    write_user(usr)

def set_connections(email, count):
    usr = read_user(email)
    usr.connection_count = count
    write_user(usr)

def get_users():
    usrs = []
    dir = os.fsencode(_base_dir)

    for file in os.listdir(dir):
        filename = os.fsdecode(file)
        if filename.endswith(".usr"): 
            path = _base_dir + filename
            usr = read_user_from_path(path)
            usrs.append(usr)

    return usrs

def write_user(usr):
    path = _base_dir + usr.email + ".usr"
    file = open(path, "w")
    json.dump(usr.serialize(), file)
    file.close()

def read_user_from_path(path):
    file = open(path, "r")
    j = json.load(file)
    file.close()

    return Visitor(j['email'], j['order_count'], j['sources'], j['connection_count'], j['gender'], j['locale'])


def read_user(email):
    try:
       return read_user_from_path(_base_dir + email + ".usr")
    except FileNotFoundError:
        return Visitor(email, 0, None, 0, 'Unknown', 'Unknown')

pattern = re.compile(r'^(\S+)@(\S{1,2}).*?(\.[^.]+)$') # obfuscating the email

def _obfuscate(email):
    pos = email.find("@")
    domain = email[pos:]
    if pos > 2:
        pos = 2

    email = "{}********{}".format(email[0:pos], domain)

    return email