from auth0.pizza42.pizza import Pizza

all_pizzas = [
    Pizza("pepperoni", "Pepperoni Pizza", "Spicy pepperoni", 8.42),
    Pizza("napoli", "Pizza Napoli", "Anchovies, capers", 10.42),
    Pizza("margherita", "Pizza Margherita", "Mozzarella cheese, fresh basil", 7.42),
    Pizza("hawaii", "Hawaii Pizza", "Canadian bacon, pineapple", 8.42),
]
