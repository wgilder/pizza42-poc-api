import http.client
import json
import urllib.parse

def get_access_token():
    conn = http.client.HTTPSConnection("wgilder.eu.auth0.com")
    payload = "{\"client_id\":\"VPEtep35zff60WUCA7jk736hePXd4rVH\",\"client_secret\":\"A0AU_BkSWc_l9FD-RF39m9BX7fI45OpMzLkumhnz-Xdxyx6FywCPe5gKmgIe9tNr\",\"audience\":\"https://wgilder.eu.auth0.com/api/v2/\",\"grant_type\":\"client_credentials\"}"
    headers = { 'content-type': "application/json" }
    conn.request("POST", "/oauth/token", payload, headers)
    res = conn.getresponse()
    data = res.read()
    return json.loads(data.decode("utf-8"))["access_token"]

def get_user_profile(token, id):
    conn = http.client.HTTPSConnection("wgilder.eu.auth0.com")

    auth = "Bearer %s" % token

    headers = { 'authorization': auth }

    dest = "/api/v2/users/%s" % id
    conn.request("GET", dest, headers=headers)

    res = conn.getresponse()
    data = res.read()

    return data.decode("utf-8")

def get_connection_count(token):
    conn = http.client.HTTPSConnection("people.googleapis.com")
    auth = "Bearer %s" % token
    headers = { 'authorization': auth }
    dest = "/v1/people/me/connections?pageSize=1&personFields=genders"
    conn.request("GET", dest, headers=headers)

    res = conn.getresponse()
    data = res.read()

    return json.loads(data.decode("utf-8"))["totalPeople"]
