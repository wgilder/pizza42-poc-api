import os

class VersionView(object):
    def __init__(self):
        super().__init__()

    def message(self):
        return "This space intentionally left blank"

    def version(self):
        return "0.1.0"
    
    def build_number(self):
        if ("P42_API_ENV" in os.environ):
            self._build_number = os.environ["P42_API_ENV"] or "-1"
        else:
            self._build_number = "-1"

    def author(self):
        return "Walter Gildersleeve"

    def email(self):
        return "walter@gildersleeve.eu"

    def serialize(self):
        return { 'message': self.message(), 'version': self.version(), 'build_number': self.build_number(), 'author': self.author(), 'email': self.email() }
